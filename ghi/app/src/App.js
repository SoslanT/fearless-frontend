import Nav from './Nav'
import PresentationForm from './PresentationForm'
import ConferenceForm from './ConferenceForm'
import AttendeesList from './AttendeesList'
import LocationForm from './LocationForm'
import AttendConferenceForm from './AttendConferenceForm'
import { BrowserRouter, Routes, Route } from 'react-router-dom'

function App(props) {
  if (props.attendees === undefined) {
    return null
  }
  return (
    <BrowserRouter>
      <Nav />

      <div className="container">
        <Routes>
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />} />
          </Route>
          <Route path="presentations">
            <Route path="new" element={<PresentationForm />} />
          </Route>

          {/* <AttendeesList attendees={props.attendees} /> */}
        </Routes>
      </div>
    </BrowserRouter>
  )
}

export default App
